function chooseLanguage() {
	$('#language').hide();
	language = $("#selectLanguage :selected").text();
	if (language == "English") {
		var question = document.getElementById ("questionEn") ;
		question.style.visibility = "visible";
		$('#questionDe').hide();
	} else if (language == "German") {
		var question = document.getElementById ("questionDe") ;
		question.style.visibility = "visible";
		$('#questionEn').hide();
	}
}

function answerQuestion() {
	var answer = ($('input[name="location"]:checked').val());
	if (answer == "1") {
		if (language == "English") {
			swal("Well done, gomo's head office is in Brighton");
			var extraInfo = document.getElementById ("extraInfoEn") ;
			extraInfo.style.visibility = "visible" ;
		} else if (language == "German") {
			swal("Gut gemacht, gomos Hauptsitz ist in Brighton");
			var extraInfo = document.getElementById ("extraInfoDe") ;
			extraInfo.style.visibility = "visible" ;
		}
	} else {
		if (language == "English") {
			swal("This is not the correct answer. Try again!");
		} else if (language == "German") {
			swal("Das ist nicht die richtige Antwort. Versuch es noch einmal!");
		}
	}
}